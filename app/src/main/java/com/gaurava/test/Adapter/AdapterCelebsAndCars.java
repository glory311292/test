package com.gaurava.test.Adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.gaurava.test.Activity.MainActivity;
import com.gaurava.test.Model.CelebandCarsData;
import com.gaurava.test.R;
import com.gaurava.test.Utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterCelebsAndCars extends RecyclerView.Adapter<AdapterCelebsAndCars.ViewHolder> {
    private static final String TAG = AdapterCelebsAndCars.class.getSimpleName();
    private Context context;
    private ArrayList<CelebandCarsData> listData;
    private ArrayList<String> keys;
    private final int VIEW_TYPE_CELEB = 1;
    private final int VIEW_TYPE_CAR = 2;

    public AdapterCelebsAndCars(Context context, ArrayList<CelebandCarsData> valueData, ArrayList<String> keyData) {
        this.listData = valueData;
        this.keys = keyData;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (TextUtils.isEmpty(listData.get(position).age)){
            return VIEW_TYPE_CAR;
        }
        else{
            return VIEW_TYPE_CELEB;
        }
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View viewItem;
        if (viewType == VIEW_TYPE_CELEB){
            viewItem = layoutInflater.inflate(R.layout.item_celebs, parent, false);
        }
        else{
            viewItem = layoutInflater.inflate(R.layout.item_car, parent, false);
        }

        return new ViewHolder(viewItem, viewType);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_CELEB){
            if (position == 0){
                holder.celebHeader.setVisibility(View.VISIBLE);
            }
            else{
                holder.celebHeader.setVisibility(View.GONE);
            }

            holder.celebName.setText(keys.get(position));
            holder.celebAge.setText("AGE: "+listData.get(position).age+" Years");
            holder.celebHeight.setText("HEIGHT: "+listData.get(position).height+" CM");
            Utils.setImageWithPicasso(context, listData.get((position)).photo, holder.celebImage, true, R.mipmap.ic_launcher);
        }
        else{
            if (getItemViewType(position-1) == VIEW_TYPE_CELEB){
                holder.carHeader.setVisibility(View.VISIBLE);
            }
            else{
                holder.carHeader.setVisibility(View.GONE);
            }

            holder.carName.setText(keys.get(position));
            Utils.setImageWithPicasso(context, listData.get((position)).photo, holder.carImage, true, R.mipmap.ic_launcher);
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView celebName, celebAge, celebHeight, celebHeader, carHeader,  carName;
        ImageView celebImage, carImage;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            if (viewType == VIEW_TYPE_CELEB) {
                celebHeader = itemView.findViewById(R.id.celebHeader);
                celebName = itemView.findViewById(R.id.celebName);
                celebAge = itemView.findViewById(R.id.celebAge);
                celebHeight = itemView.findViewById(R.id.celebHeight);
                celebImage = itemView.findViewById(R.id.celebImage);
            }
            else {
                carHeader = itemView.findViewById(R.id.carHeader);
                carName = itemView.findViewById(R.id.carbName);
                carImage = itemView.findViewById(R.id.carImage);
            }
        }
    }
}


