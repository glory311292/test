package com.gaurava.test.Retrofit;

import com.gaurava.test.Model.CelebsAndCars;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("/bins/1gaa29")
    Call<CelebsAndCars> getCelebsAndCars();
}