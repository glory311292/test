package com.gaurava.test.Retrofit;


import com.gaurava.test.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public static Retrofit retrofit = null;
    private static ApiInterface apiInterface = null;

    public static ApiInterface getInstance() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASEURL)
                    .client(provideOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        if (apiInterface == null) {
            apiInterface = retrofit.create(ApiInterface.class);
        }
        return apiInterface;
    }

    //Creating OKHttpClient
    private static OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        if (BuildConfig.SHOULD_SHOW_LOG){
            logging.level(HttpLoggingInterceptor.Level.BODY);
        }
        else{
            logging.level(HttpLoggingInterceptor.Level.NONE);
        }

        return new OkHttpClient.Builder()
                .readTimeout(480, TimeUnit.SECONDS)
                .connectTimeout(480, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();
    }
}
