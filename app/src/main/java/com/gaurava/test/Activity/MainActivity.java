package com.gaurava.test.Activity;

import android.app.Dialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gaurava.test.Adapter.AdapterCelebsAndCars;
import com.gaurava.test.Model.CelebandCarsData;
import com.gaurava.test.Model.CelebsAndCars;
import com.gaurava.test.R;
import com.gaurava.test.Retrofit.RetrofitClient;
import com.gaurava.test.TestApplication;
import com.gaurava.test.Utils.LogUtil;
import com.gaurava.test.Utils.UIUtils;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private Dialog dialog;

    AdapterCelebsAndCars adapterCelebsAndCars;
    private ArrayList<CelebandCarsData> allData = new ArrayList<>();
    private ArrayList<String> keys = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        adapterCelebsAndCars = new AdapterCelebsAndCars(MainActivity.this, allData, keys);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView.setAdapter(adapterCelebsAndCars);


        getCelebsAndCarsData();
    }

    private void getCelebsAndCarsData() {
        if (!TestApplication.hasNetwork()){
            UIUtils.showToast("No internet connection");
            return;
        }
        dialog = UIUtils.showProgressDialog(this, "Loading the resources..");
        Call<CelebsAndCars> call = RetrofitClient.getInstance().getCelebsAndCars();
        call.enqueue(new Callback<CelebsAndCars>() {
            @Override
            public void onResponse(@NotNull Call<CelebsAndCars> call, @NotNull Response<CelebsAndCars> response) {
                dialog.dismiss();
                LogUtil.showErrorLog("Success", "NETWORK CODE: " + response.code()+" : NETWORK MESSAGE: "+response.message());
                if (response.code()!=500) {
                    if(response.code()==200) {
                               LogUtil.showErrorLog("Success", "RESPONSE: " + new Gson().toJson(response.body()));
                        if (response.isSuccessful() && response.body() != null) {
                            LogUtil.showErrorLog("Celebs", " "+response.body().celebrities.keySet().toString());
                            LogUtil.showErrorLog("Cars", " "+response.body().cars.keySet().toString());
                            allData.addAll(new ArrayList<CelebandCarsData>(response.body().celebrities.values()));
                            keys.addAll(new ArrayList<String>(response.body().celebrities.keySet()));
                            allData.addAll(new ArrayList<CelebandCarsData>(response.body().cars.values()));
                            keys.addAll(new ArrayList<String>(response.body().cars.keySet()));
                            adapterCelebsAndCars.notifyDataSetChanged();

                        } else {
                            UIUtils.showToast(response.message());
                        }
                    }
                    else{
                        UIUtils.showToast("ERROR: "+ response.message());
                    }
                }
                else{
                    LogUtil.showErrorLog("Success", "RESPONSE 500: " + response.message());
                    UIUtils.showToast("Something went wrong, please try again later!");
                }
            }

            @Override
            public void onFailure(@NotNull Call<CelebsAndCars> call, @NotNull Throwable t) {
                dialog.dismiss();
                LogUtil.showErrorLog("Failure", ""+t.getMessage());
            }
        });
    }
}
