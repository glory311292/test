package com.gaurava.test.Utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;

import com.gaurava.test.R;
import com.gaurava.test.TestApplication;

public class UIUtils {

    public static Dialog showProgressDialog(Context context, String loaderMessage) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_progress);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = convertDpToPixel(300, context);
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        try {
            ((AppCompatTextView)dialog.findViewById(R.id.tvMessage)).setText(loaderMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.show();

        return dialog;
    }

    public static void showToast(String message){
        if (!TextUtils.isEmpty(message)) {
            try {
                Toast.makeText(TestApplication.getInstance().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                LogUtil.showErrorLog("TOAST", "Message: " + message.toUpperCase());
            }
        }
        else{
            LogUtil.showErrorLog("TOAST", "Message: NO MESSAGE SENT");
        }
    }

    public static int convertDpToPixel(float dp, Context context) {
        return (int) (dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
