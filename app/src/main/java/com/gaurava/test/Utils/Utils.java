package com.gaurava.test.Utils;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

public class Utils {
    public static void setImageWithPicasso(Context context, String path, ImageView imageView, boolean pleceHolderNeede, int placeholderId){
        try {
            if (pleceHolderNeede) {
                Picasso.with(context)
                        .load(path)
                        .placeholder(placeholderId)
                        //.memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .into(imageView);
            } else {
                Picasso.with(context)
                        .load(path)
                        //.memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                        .into(imageView);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }


        /*try{
            Picasso.with(context).invalidate(path);
        }
        catch (Exception e){
            e.printStackTrace();
        }*/
    }
}
