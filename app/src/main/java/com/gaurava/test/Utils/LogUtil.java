package com.gaurava.test.Utils;

import android.util.Log;

import com.gaurava.test.BuildConfig;


public class LogUtil {
    public static void showErrorLog(String tag, String message) {
        try {
            if (BuildConfig.SHOULD_SHOW_LOG) {
                Log.e(tag, message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}